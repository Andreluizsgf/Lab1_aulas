#include <bits/stdc++.h>

using namespace std;

int main(){
  int n;
  cin >> n;
  int **M;

  M = (int**)malloc(sizeof(int*)*n);

  for(int i = 0 ; i < n ; i++) {
    M[i] = (int*)malloc(sizeof(int)*n);
  }

  for (size_t i = 0; i < n ; i++) {
    for (size_t j = 0; j < n ; j++) {
      M[i][j] = 10;
    }
  }

  for (size_t i = 1; i < n ; i++) {
    for (size_t j = 1; j < n ; j++) {
    cout << '_' << M[i][j];
    }
    cout << endl;
  }

  return 0;
}
