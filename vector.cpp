#include <vector>
#include <iostream>

int main()
{
  vector <int> v;
  int n;
  while (cin >> n && n! = 0)
    v.push_back(n);

    for (int i = 0 ; i < v.size() ; i++){
      cout << v[i] << endl;
    }

    vector <int>::iterator it;
    for (it = v.begin() ; it != v.end() ; it++){
      cout << *it << endl;
    }

    v.erase(v.begin());
    v.erase(v.begin()+2, v.end()); //estou ignorando as 2 primeiras posições.
    v.clear(); //apagar todos o vetor

}
