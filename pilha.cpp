#include <iostream>
#include <stack>

using namespace std;

int main()
{
  stack <int>pilha; //<tipo das variaveis> e nome da pilha
  int n;
  for (int i = 0 ; i < 3 ; i++) {
    cin >> n; //entrando com os valores
    pilha.push(n); //colocando os valores na pilha.
  }

  while(!pilha.empty()) {
    cout << pilha.top() << endl;
    pilha.pop();
  }
}
