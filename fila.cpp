#include <iostream>
#include <stack>
#include <queue>

using namespace std;

struct dados
{
  int a;
  char b;
};

int main()
{
  queue <dados>fila; //<tipo das variaveis> e nome da fila
  dados n;
  for (int i = 0 ; i < 3 ; i++) {
    cin >> n.a >> n.b; //entrando com os valores
    fila.push(n); //colocando os valores na pilha.
  }

  while(!fila.empty()) {
    cout << fila.front() << endl;
    fila.pop();
  }
}
