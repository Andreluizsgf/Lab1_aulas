#include <set>
#include <iostream>

using namespace std;

int main()
{
  set <float> S;
  float num;

  while (cin >> num){
    S.insert(num);
  }

  set <float>::iterator it;
  for (it = S.begin() ; it != S.end() ; it++)
    cout << *it << endl;

  it = S.find(20);
  if(it!=S.end()) S.erase(it);
  if(S.count(20) != 0) S.erase(20);
}

//Elementos já estão ordenados e não tem repetição.
