#include <bits/stdc++.h>

using namespace std;

int main(){
  int n;
  cin >> n;
  int *v;
  v = (int*) malloc (sizeof(int)*n);
  for (int i = 0 ; i < n ; i++) {
    v[i] = i;
  }

  for (int i = 0 ; i < n ; i++) {
    cout << i << '_' << v[i] << endl;
  }
}
