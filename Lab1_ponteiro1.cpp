#include <bits/stdc++.h>

using namespace std;

int main()
{
  int a,b;
  int *p,*p1; /*Declaração de ponteiro. O ponteiro é usado para fazer linkagens. O ponteiro aponta para algum local da memoria.
  */
  a=20;
  b=30;
  p=&a;
  p1=&b;
  *p=*p1; /*Para pegar o endereço é só usar o &.*/
  p=p1;
  (*p)++;


}
