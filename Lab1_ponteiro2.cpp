#include <bits/stdc++.h>

using namespace std;

void troca (int *n1,int *n2) {
  int aux;
  aux = *n1;
  *n1 = *n2;
  *n2 = aux;
}

int main()
{
  int n1 = 10 , n2 = 20;
  troca(&n1,&n2);
  cout << n1 << '_' <<  n2 << endl;



}
